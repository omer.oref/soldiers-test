import { initializeApp } from "firebase/app";
import { getDatabase, onValue, ref as dbRef, set, push, remove } from "firebase/database";
import { getAuth, onAuthStateChanged, createUserWithEmailAndPassword, signInWithEmailAndPassword, updateProfile, signOut } from "firebase/auth";
import { getStorage, ref as sRef, getDownloadURL } from "firebase/storage";

import store from './store'

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyBzmtrxkxOh3Ntc56i9pt9Z3PpM5vu8VgI",
  authDomain: "soldiers-test.firebaseapp.com",
  databaseURL: "https://soldiers-test-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "soldiers-test",
  storageBucket: "soldiers-test.appspot.com",
  messagingSenderId: "934414445591",
  appId: "1:934414445591:web:45e44e62f84d818cd34052"
};
initializeApp(firebaseConfig);

// Database
const db = getDatabase()
const unitsRef = dbRef(db, 'units')
onValue(unitsRef, (snapshot) => {
  const data = snapshot.val()
  store.state.units = data
  store.dispatch('initChapterStatuses')
  store.state.fetchedUnits = true
})

export function addUnit(title) {
  const newUnitRef = push(unitsRef);
  set(newUnitRef, {
    title: title,
    chapters: null,
    id: newUnitRef.key
  })
}

export function deleteUnit(unitid) {
  const chapterRef = dbRef(db, 'units/'+unitid)
  remove(chapterRef)
}

export function addChapter(unitid, {title, type, content}) {
  const chaptersRef = dbRef(db, 'units/'+unitid+'/chapters')
  const newChapterRef = push(chaptersRef)
  set(newChapterRef, {
    title: title,
    id: newChapterRef.key,
    type: type || 'text',
    content: content
  })
}

export function deleteChapter(unitid, chapterid) {
  const chapterRef = dbRef(db, 'units/'+unitid+'/chapters/'+chapterid)
  remove(chapterRef)
}

export function editChapter(unitid, {id, title, type, content}) {
  const chapterRef = dbRef(db, 'units/'+unitid+'/chapters/'+id)
  set(chapterRef, {
    title: title,
    id: id,
    type: type || 'text',
    content: content
  })
}

// Users Auth
const auth = getAuth()

onAuthStateChanged(auth, (user) => {
  if (user) {
    store.state.user = user
    store.state.username = auth.currentUser.displayName
  } else {
    store.state.user = null
    store.state.username = null

  }
})


export async function signUp(name, email, password) {
  return createUserWithEmailAndPassword(auth, email, password)
  .then(() => {
    updateProfile(auth.currentUser, {
      displayName: name
    })
    .then(() => {
      store.state.username = auth.currentUser.displayName
    })
  })
}

export async function logIn(email, password) {
  return signInWithEmailAndPassword(auth, email, password)
}

export async function logOut() {
  store.state.isEditing = false
  return signOut(auth)
}

// Cloud Storage
const storage = getStorage()

const podcastsRefDB = dbRef(db, 'podcasts')
onValue(podcastsRefDB, (snapshot) => {
  const data = snapshot.val()
  store.state.podcasts = data
  store.state.fetchedPodcasts = true
})

export async function getStorageFile(file) {
  const fileRef = sRef(storage, 'podcasts/'+file)
  return getDownloadURL(fileRef)
  .then((url) => {
    return url
  })
}

export function getDriveLink(url) {
  const fileID = url.split('/')[5]
  return "https://docs.google.com/uc?export=download&id="+fileID
}

export async function addPodcast(name, link) {
  const newPodcastRef = push(podcastsRefDB)
  set(newPodcastRef, {
    id: newPodcastRef.key,
    name: name,
    link: link
  })
}

export function deletePodcast(podcastid) {
  const podcastRef = dbRef(db, 'podcasts/'+podcastid)
  remove(podcastRef)
}

