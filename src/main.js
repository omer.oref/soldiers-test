import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VWave from 'v-wave'
import easySpinner from 'vue-easy-spinner';
import CKEditor from '@ckeditor/ckeditor5-vue';

createApp(App).use(store).use(router).use(VWave).use(easySpinner, {
  prefix: 'easy',
}).use(CKEditor).mount('#app')
